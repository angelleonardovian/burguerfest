
package Controlador;

import Negocio.Concurso;
import Negocio.UsuarioBO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class InicioUser extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            if (request.getSession().getAttribute("concurso") == null) {
                request.getSession().setAttribute("error", "No existe concurso creado");
                request.getRequestDispatcher("./punto3/jsp/error.jsp").forward(request, response);                
                return;
                
            }
            
            String email = request.getParameter("email");
            String contraseña = request.getParameter("contraseña");            
            
            Concurso concurso = (Concurso) request.getSession().getAttribute("concurso");            
            UsuarioBO user = concurso.validarLoginUsuario(contraseña, email);
            
            if(user == null){
                request.getRequestDispatcher("./punto3/jsp/error.jsp").forward(request, response);
            }
            else{
                request.getSession().setAttribute("user", user);
                request.getRequestDispatcher("./punto3/html/prueba.html").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
