package Negocio;

import java.util.TreeSet;

public class Concurso {

    public final TreeSet<UsuarioBO> usuarios = new TreeSet();

    public Concurso() {
    }

    public boolean insertarUsuario(String nombre, String email, String contraseña) {
        if (validarRegistroUsuario(email, contraseña)) {
            UsuarioBO nuevo = new UsuarioBO();
            nuevo.setNombre(nombre);
            nuevo.setEmail(email);
            nuevo.setContraseña(contraseña);
            return this.usuarios.add(nuevo);
        }
        return false;
    }

    private UsuarioBO buscarUsuario(String email) {
        UsuarioBO x = new UsuarioBO();
        x.setEmail(email);
        if (this.usuarios.contains(x)) {
            return this.usuarios.floor(x);
        }
        return null;
    }

    private boolean validarRegistroUsuario(String email, String contraseña) {
        String emailS[] = email.split("@");
        return emailS[1].equals("gmail.com") && contraseña.length() == 7;
    }
    
    public UsuarioBO validarLoginUsuario(String contraseña, String email) {
        UsuarioBO user = buscarUsuario(email);
                
        if(user.getContraseña().equals(contraseña))
            return user;
        else
            return null;
    }
}
