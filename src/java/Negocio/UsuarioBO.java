
package Negocio;

import Dto.Usuario;

public class UsuarioBO implements Comparable{
    
    private Usuario usuario;

    public UsuarioBO() {
        usuario = new Usuario();
    }
        
    
    @Override
    public int compareTo(Object o) {
       UsuarioBO x=(UsuarioBO)o;
       
     return ((int)(getEmail().hashCode())-x.getEmail().hashCode());
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioBO other = (UsuarioBO) obj;
        if (getEmail() != other.getEmail()) {
            return false;
        }
        return true;
    }  
    
    @Override
    public String toString() {
        return "Usuario{ " + "nombre=" + getNombre() +         
                "Email=" + getEmail() + " }";
    }
    
    public String getNombre() {
        return usuario.getNombre();
    }

    public void setNombre(String nombre) {
        usuario.setNombre(nombre);
    }

    public String getEmail() {
        return usuario.getEmail();
    }

    public void setEmail(String email) {
        usuario.setEmail(email);
    }

    public String getContraseña() {
        return usuario.getContraseña();
    }

    public void setContraseña(String contraseña) {
        usuario.setContraseña(contraseña);
    }
    
}
