<%-- 
    Document   : registroexitoso
    Created on : 12/11/2019, 08:30:15 AM
    Author     : Leonardo
--%>

<%@page import="Negocio.UsuarioBO"%>
<%@page import="Negocio.Concurso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String msg = (String) (request.getSession().getAttribute("msg"));
            UsuarioBO user = (UsuarioBO) (request.getSession().getAttribute("user"));            
        %>
        
        <h1>Gracias por participar:</h1>
        
        <p>Nombre: <%= user.getNombre()%></p>
        <p>UD: <%=msg%> el concurso.</p>
        <a href="./index.html">Ir a menú principal</a>
        
        
    </body>
</html>
